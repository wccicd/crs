pipeline {
  agent any
  triggers {
    pollSCM('* * * * *')
  }
  stages {
    stage('Build') {
      stages {
        stage('Git Auto Tagging') {
          when {
                branch 'master'
          }
          steps {
            sshagent (credentials: ['gitlabjenkins']) {
                sh("./gitautotag.sh")
            }
          }
        }
        stage('WCB') {
          steps {
            sh '''
              #stop and remove utility docker
              docker stop ${BUILD_IMAGE} || true
              docker rm ${BUILD_IMAGE} || true
              #docker rmi "${DOCKER_REG}/${DOCKER_NAMESPACE}/${BUILD_IMAGE}:${BUILD_IMAGE_TAG}" || true
            '''
            sh 'echo "${DOCKER_PASSWORD}" | docker login ${DOCKER_REG} -u ${DOCKER_USER} --password-stdin'
            sh '''set -x
              ## Pull the build image:
              set +e
              docker pull "${DOCKER_REG}/${DOCKER_NAMESPACE}/${BUILD_IMAGE}:${BUILD_IMAGE_TAG}" > /dev/null 2>&1
              ERROR_CODE=$?
              set -e
              if [ "$ERROR_CODE" -ne "0" ]; then
                echo "Error while pulling build docker image.  Exiting."
                exit -1
              fi
              # Start build utility container:
              docker run -d -e LICENSE=accept --name ${BUILD_IMAGE} "${DOCKER_REG}/${DOCKER_NAMESPACE}/${BUILD_IMAGE}:${BUILD_IMAGE_TAG}"

              # Create directory for source files:
              docker exec ${BUILD_IMAGE} bash -c "mkdir -p /opt/WebSphere/CommerceServer90/src/${BUILD_TYPE}"

              # Copy custom code for image we are building into the build container:
              docker cp . ${BUILD_IMAGE}:/opt/WebSphere/CommerceServer90/src/${BUILD_TYPE}

              # Execute build of customization package inside of the build container:
              docker exec ${BUILD_IMAGE} bash -c "cd /opt/WebSphere/CommerceServer90/wcbd && ./build.sh ${CUSTCODE} ${BUILD_TYPE} ${BUILD_NUMBER} ${DROP_LOC}"

              # Copy the bundle zip from the build container back into the jenkins workspace:
              bundle_fname=wcbd-deploy-server-${CUSTCODE}-${BUILD_TYPE}-${BUILD_NUMBER}.zip
              docker cp ${BUILD_IMAGE}:/tmp/$bundle_fname docker-build/${BUILD_TYPE}
              # Copy the logs to log dir
              docker cp ${BUILD_IMAGE}:/opt/WebSphere/CommerceServer90/wcbd/logs ./logs
            '''
            archiveArtifacts artifacts: 'logs/**/*.log', fingerprint: true
          }
        }
        stage('Docker Build') {
          when {
                branch 'master'
          }
          steps {
            sh 'docker-build/docker-build.sh $BUILD_TYPE $BUILD_NUMBER $IMAGE_TYPE $CUSTCODE > logs/docker-build-${CUSTCODE}-${BUILD_TYPE}-${BUILD_NUMBER}.log'
            archiveArtifacts artifacts: 'logs/**/*.log', fingerprint: true
          }

        }
        stage('Push to GitOps') {
          when {
                branch 'master'
          }
          steps {
            sshagent (credentials: ['jenkins-generated-ssh-key']) {
            sh '''
              set -x
              if [ "$GITOPS_PUSH" = true ]; then
                if [ -d ${GIT_HELM_REPO_BASE_DIR} ]; then
                   rm -rf ${GIT_HELM_REPO_BASE_DIR}
                fi
                git clone "${GIT_HELM_REPO_URL}"

                cd ${GIT_HELM_REPO_BASE_DIR}
                # Update values.yaml to have new build number
                fname=${AUTH_DIR}/values.yaml
                sed -i "s/${CUSTCODE}-${BUILD_TYPE}-.*$/${CUSTCODE}-${BUILD_TYPE}-${BUILD_NUMBER}/" ${fname}
                fname=${LIVE_DIR}/values.yaml
                sed -i "s/${CUSTCODE}-${BUILD_TYPE}-.*$/${CUSTCODE}-${BUILD_TYPE}-${BUILD_NUMBER}/" ${fname}

                git add ${AUTH_DIR}/values.yaml
                git add ${LIVE_DIR}/values.yaml
                git config --global user.email "wccicd@test.com"
                git config --global user.name "wccicd"
                git commit -m "updated values.yaml to use BUILD NUMBER: ${BUILD_NUMBER} build tag"
                git push
                cloudctl login -a https://$ICP_HOST:8443 --skip-ssl-validation -u $ICP_USER -p $ICP_PW -n commerce
                helm upgrade demoqaauth ${AUTH_DIR} --tls 
              fi
            '''
            }
          }
        }
      }
    }
  }
  environment {
    BUILD_IMAGE = 'wc-build'
    BUILD_IMAGE_TAG = 'latest'
    BUILD_TYPE = 'crs'
    BUNDLE_DIR = '../../bundle-zips'
    CUSTCODE = 'dem'
    DOCKER_NAMESPACE = 'commerce'
    DOCKER_PASSWORD = 'admin'
    DOCKER_REG = 'mycluster.icp:8500'
    DOCKER_USER = 'admin'
    DOSFTP = 'false'
    DROP_LOC = '/tmp'
    GIT_DOTAGS = 'true'
    GITOPS_PUSH = 'true'
    IMAGE_TYPE = 'crs-app'
    SLACK_URL="false"
    WC9_VERSION = '9.0.1.5'
    WCBD_BUNDLE_NAME = 'wcbd_files.zip'
    GIT_HELM_REPO_EMAIL = 'wccicd@test.com'
    GIT_HELM_REPO_USERNAME = 'wccicd'
    GIT_HELM_REPO_PW = 'passw0rd'
    GIT_HELM_REPO_BASE_DIR = 'helmcharts' 
    GIT_HELM_REPO_URL = 'ssh://git@testgit.localdomain:30022/wccicd/helmcharts.git'
    AUTH_DIR = 'auth'
    LIVE_DIR = 'live'
    ICP_USER = 'admin'
    ICP_PW = 'admin'
    ICP_HOST = 'mycluster.icp'
  }
  post {
    success {
      sh '''
        #stop and remove utility docker
        docker stop ${BUILD_IMAGE}
        docker rm ${BUILD_IMAGE}
        #docker rmi "${DOCKER_REG}/${DOCKER_NAMESPACE}/${BUILD_IMAGE}:${BUILD_IMAGE_TAG}"
      '''
    }
    failure {
      sh '''
        if [[ "${SLACK_URL}" != "false" ]]
        then
          MESSAGE="BUILD FAILED! Please go here ${RUN_DISPLAY_URL}"
          curl -X POST -H 'Content-type: application/json' --data "{'text':\\"${MESSAGE}\\"}" ${SLACK_URL}
        fi
      '''
    }
  }
}
