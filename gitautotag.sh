#!/bin/bash

#set -x
set -o errexit
set -o nounset

if [[ "$GIT_DOTAGS" = true ]]
then
  #tag files in git with build number:
  CC=${CUSTCODE}-${BUILD_TYPE}
  BUILD_LABEL=${CC}-${BUILD_NUMBER}
  git tag $BUILD_LABEL
  #### Kept for LEGACY, needed for JAZZHUB, don\'t need in bluemix
  #git remote set-url origin https://$BLUEMIX_USER:$BLUEMIX_PASS@$BLUEMIX_URL/$BLUEMIX_PROJ > /dev/null 2>&1
  git push --tags -q origin  || true
fi