# WC V9 WCB Cloud Devops

This repo contains all the combined WCBD scripts that have been used in Cloud Devops Services.
But, being modified to work with V9.


Scripts have the following features:
* Git auto tagging
* DailyJob deploy (Work in Progress TODO)
* Httpd Configure Deploy (Work in Progress TODO)

Overview of each:

## Git AutoTag
When you run the `build.sh` or `build-search.sh` scripts.

You send in a Customer Code. This is usually a 3 char code, just for reference.  This is simply used to generate the Deploy package with a unique name.

```
 
./build.sh cust {ts, search, xc, or crs}
```
passing in $1 the custcode will set the `$CC` variable.
passing in $2 the app.type will sett the `$BUILD_TYPE` variable.

The script will then take these *Environment variables*.

`$BUILD_NUMBER` , this gets generated by your build server, and should get incremented for every build. Note: Bluemix creates this automatically for you.

`$BLUEMIX_DOTAGS` , this is a boolean variable that tells the scripts to do the auto tagging or not.

With auto tagging, every build will be tagged with

`$BUILD_LABEL` which is `$CC-$BUILD-NUMBER`

Here is an example.
cust-48

Using git auto-tagging, you can conceivably go back and get any build in the past.

## DailyJob deploy (WIP TODO)
If your project uses the DailyJob.  Then you can create customizations in the DailyJob folder and they will be deployed.

## Httpd Configure Deploy (WIP TODO)

Simply copies files in `httpdconf.module.name=WebSphereHTTPServerConfig` to the HTTP Conf directory specified in the deploy properties.

**Does not do a HTTP restart.**

## Reference

[IBM Cloud Redbook](http://www.redbooks.ibm.com/redbooks/pdfs/sg248374.pdf)
Here is a Redbook on how to setup IBM Cloud Devops Services.

test1

<<<<<<< HEAD

=======
>>>>>>> 2471d8c60288cb9a57b75d2e00376bf0e7e13c64
